package routers

import (
	"aptirsapi/controller"
	"aptirsapi/database"
	"aptirsapi/middleware"
	"aptirsapi/utils"
	"github.com/gin-gonic/gin"
	jwt "github.com/kyfk/gin-jwt"
	"net/http"
	"regexp"
	"strconv"
	"strings"
)

func GetRouters() (*gin.Engine, error) {
	dsn, _ := utils.GetEnv("DSN")
	dsnType, _ := utils.GetEnv("DSNTYPE")
	sourcesStr, _ := utils.GetEnv("DSNMAXSOURCES")
	sources, err := strconv.Atoi(sourcesStr)
	if err != nil || sources < 1 {
		sources = 1
	}
	database.InitDB(dsn, dsnType, sources)
	var db = database.GetDB()
	database.SeedAll(db)
	addr, _ := utils.GetEnv("REDIS_ADDRESS")
	psw, _ := utils.GetEnv("REDIS_PASSWORD")
	redisDbStr, _ := utils.GetEnv("REDIS_DB")
	redisDb, err := strconv.Atoi(redisDbStr)
	if err != nil {
		redisDb = 0
	}
	database.InitCache(addr, psw, redisDb)
	auth, err := middleware.NewAuth(db)
	if err != nil {
		return nil, err
	}
	router := gin.Default()
	router.Use(jwt.ErrorHandler)
	/*router.Use(cors.New(cors.Config{
		AllowMethods:     []string{"GET", "POST", "PUT", "PATCH", "DELETE", "HEAD", "OPTIONS"},
		AllowHeaders:     []string{"Origin", "Content-Length", "Content-Type"},
		AllowCredentials: false,
		MaxAge:           12 * time.Hour,
		AllowOrigins:     []string{"http://127.0.0.1:8081"},
	}))*/

	r := router.Group("/api")
	r.GET("/", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"data": "hello world"})
	})

	r.POST("/register", func(c *gin.Context) {
		controller.CreateUserAuth(c)
	})
	r.POST("/login", func(c *gin.Context) {
		auth.Authenticate(c)
	})
	r.GET("/refresh_token", middleware.IsUser(auth), auth.RefreshToken)
	r.GET("/check", middleware.IsUser(auth), func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"data": "hello world"})
	})
	r.GET("/testuser", middleware.IsAdmin(auth), func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{"data": "hello world"})
	})

	admin := r.Group("/admin")
	admin.GET("/roleslist", middleware.IsAdmin(auth), controller.GetRoles)
	users := admin.Group("/users")
	users.GET("/", middleware.IsAdmin(auth), controller.GetUsers)
	users.GET("/:user", middleware.IsAdmin(auth), controller.GetUser)
	users.PUT("/:user", middleware.IsAdmin(auth), controller.UpdateUser)
	users.DELETE("/:user", middleware.IsAdmin(auth), controller.RemoveUser)

	clients := r.Group("/clients")
	clients.POST("/", middleware.IsManager(auth), controller.AddClient)
	clients.GET("/", middleware.IsManager(auth), controller.GetClients)
	// workaround for https://github.com/gin-gonic/gin/issues/1681
	r.GET("/clientslist", middleware.IsAnalyst(auth), controller.GetClientsList)
	clients.GET("/:client", middleware.IsManager(auth), controller.GetClient)
	clients.PUT("/:client", middleware.IsManager(auth), controller.UpdateClient)
	clients.DELETE("/:client", middleware.IsManager(auth), controller.RemoveClient)

	titles := r.Group("/titles")
	titles.POST("/", middleware.IsManager(auth), controller.AddTitle)
	titles.GET("/", middleware.IsManager(auth), func(c *gin.Context) {
		controller.GetTitles(c, false)
	})
	// workaround for https://github.com/gin-gonic/gin/issues/1681
	r.GET("/titleslist", middleware.IsAnalyst(auth), controller.GetTitlesList)

	// workaround for https://github.com/gin-gonic/gin/issues/1681
	r.GET("/clienttitle/:client", middleware.IsAnalyst(auth), func(c *gin.Context) {
		controller.GetTitles(c, true)
	})
	titles.GET("/:title", middleware.IsManager(auth), controller.GetTitle)
	titles.PUT("/:title", middleware.IsManager(auth), controller.UpdateTitle)
	titles.DELETE("/:title", middleware.IsManager(auth), controller.RemoveTitle)

	searchterms := r.Group("/searchterms")

	searchterms.GET("/:titleid", middleware.IsAnalyst(auth), controller.GetSearchTerms)
	searchterms.GET("/:titleid/*islist", middleware.IsAnalyst(auth), controller.GetSearchTerms)
	searchterms.POST("/:titleid", middleware.IsAnalyst(auth), controller.UpdateSearchTerms)

	searchterms.PUT("/:titleid", middleware.IsAnalyst(auth), controller.UpdateSearchTerms)

	links := r.Group("/links")
	links.GET("/", middleware.IsAnalyst(auth), controller.GetLinks)
	links.PUT("/", middleware.IsAnalyst(auth), controller.VerifyLink)
	links.POST("/", controller.AddLink)

	reports := r.Group("/reports")
	// workaround for https://github.com/gin-gonic/gin/issues/1681
	r.GET("/reportfields", middleware.IsManager(auth), controller.GetReportFields)
	r.GET("/export", controller.DoExport)
	reports.GET("/", middleware.IsManager(auth), controller.GetReports)
	reports.DELETE("/:report", middleware.IsManager(auth), controller.RemoveReport)
	reports.GET("/:report", middleware.IsManager(auth), controller.GetReport)
	reports.GET("/:report/*action", middleware.IsManager(auth), func(c *gin.Context) {
		reg, _ := regexp.Compile("[^a-zA-Z0-9]+")
		request := strings.ToLower(reg.ReplaceAllString(c.Param("action"), ""))
		if request == "export" {
			controller.GenExportToken(c)
		} else {
			controller.GetReport(c)
		}
	})
	reports.POST("/", middleware.IsManager(auth), controller.AddReport)

	r.GET("/healthcheck", controller.HealthCheck)

	return router, nil
}
