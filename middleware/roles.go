package middleware

import (
	"github.com/gin-gonic/gin"
	jwt "github.com/kyfk/gin-jwt"
)

const (
	RoleUser    = "ROLE_USER"
	RoleAnalyst = "ROLE_ANALYST"
	RoleManager = "ROLE_MANAGER"
	RoleAdmin   = "ROLE_ADMIN"
)

func RolesList() []string {
	return []string{RoleUser, RoleAnalyst, RoleManager, RoleAdmin}
}

func IsUser(m jwt.Auth) gin.HandlerFunc {
	return m.VerifyPerm(func(claims jwt.MapClaims) bool {
		items := []string{RoleUser, RoleAnalyst, RoleManager, RoleAdmin}
		_, found := Find(items, role(claims))
		if !found {
			return false
		}
		return true
	})
}

func IsAnalyst(m jwt.Auth) gin.HandlerFunc {
	return m.VerifyPerm(func(claims jwt.MapClaims) bool {
		items := []string{RoleAnalyst, RoleManager, RoleAdmin}
		_, found := Find(items, role(claims))
		if !found {
			return false
		}
		return true
	})
}

func IsManager(m jwt.Auth) gin.HandlerFunc {
	return m.VerifyPerm(func(claims jwt.MapClaims) bool {
		items := []string{RoleManager, RoleAdmin}
		_, found := Find(items, role(claims))
		if !found {
			return false
		}
		return true
	})
}

func IsAdmin(m jwt.Auth) gin.HandlerFunc {
	return m.VerifyPerm(func(claims jwt.MapClaims) bool {
		return role(claims) == RoleAdmin

	})
}

func role(claims jwt.MapClaims) string {
	return claims["role"].(string)
}

func Find(slice []string, val string) (int, bool) {
	for i, item := range slice {
		if item == val {
			return i, true
		}
	}
	return -1, false
}
