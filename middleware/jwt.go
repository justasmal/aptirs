package middleware

import (
	"aptirsapi/models"
	"aptirsapi/utils"
	"errors"
	jwtN "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	jwt "github.com/kyfk/gin-jwt"
	"gorm.io/gorm"
	"time"
)

func NewAuth(db *gorm.DB) (jwt.Auth, error) {
	secret, _ := utils.GetEnv("JWTSECRET")
	algo, _ := utils.GetEnv("JWTALGO")
	return jwt.New(jwt.Auth{
		SecretKey: []byte(secret),
		Authenticator: func(c *gin.Context) (jwt.MapClaims, error) {
			var req struct {
				Email    string `json:"email"`
				Password string `json:"password"`
			}
			if err := c.ShouldBind(&req); err != nil {
				return nil, jwt.ErrorAuthenticationFailed
			}

			authFound := models.User{}
			currUser := db.Where(&models.UserDTO{Email: req.Email}).First(&authFound)
			if currUser.RowsAffected < 1 {
				return nil, jwt.ErrorAuthenticationFailed
			}

			data, err := utils.ComparePassword(req.Password, authFound.Password)
			if err != nil {
				return nil, jwt.ErrorAuthenticationFailed
			}
			if data == false {
				return nil, jwt.ErrorAuthenticationFailed
			}

			return jwt.MapClaims{
				"email": authFound.Email,
				"role":  authFound.Role,
			}, nil
		},
		UserFetcher: func(c *gin.Context, claims jwt.MapClaims) (interface{}, error) {
			email, ok := claims["email"].(string)
			if !ok {
				return nil, nil
			}

			authFound := models.User{}
			currUser := db.Where(&models.UserDTO{Email: email}).First(&authFound)
			if currUser.RowsAffected < 1 {
				return nil, nil
			}
			return authFound, nil
		},
		SigningMethod: algo,
	})
}

func GenTempJWT(expire int64, action interface{}) (string, error) {
	secret, _ := utils.GetEnv("JWTSECRET")
	algo, _ := utils.GetEnv("JWTALGO")
	newToken := jwtN.New(jwtN.GetSigningMethod(algo))
	newClaims := jwtN.MapClaims{}
	now := time.Now()
	newClaims["data"] = action
	newClaims["exp"] = now.Unix() + expire
	newClaims["iat"] = now.Unix()
	newToken.Claims = newClaims
	token, err := newToken.SignedString([]byte(secret))
	return token, err
}

func ValidateTempJWT(token string) (interface{}, error) {
	secret, _ := utils.GetEnv("JWTSECRET")
	parsed, _ := jwtN.Parse(token, func(t *jwtN.Token) (interface{}, error) {
		return []byte(secret), nil
	})
	if parsed == nil {
		return nil, errors.New("invalid jwt")
	}
	claims, ok := parsed.Claims.(jwt.MapClaims)
	if !ok || !parsed.Valid {
		return nil, errors.New("invalid jwt")
	}

	return claims["data"], nil
}
