package controller

import (
	"aptirsapi/database"
	"aptirsapi/models"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	jwt "github.com/kyfk/gin-jwt"
)

func GetLinks(c *gin.Context) {
	var db = database.GetDB()
	status, _ := strconv.Atoi(c.Query("status"))
	queryUnescape, _ := url.QueryUnescape(c.Query("titles"))
	titlesArr := strings.Split(queryUnescape, ",")
	titlesArrNative := []models.ULID{}
	for _, x := range titlesArr {
		titlesArrNative = append(titlesArrNative, models.ULIDParse(x))
	}

	links := []models.Link{}
	statusFilter := func(db *gorm.DB) *gorm.DB {
		return db.Where("status = ?", status)
	}
	tiltesFilter := func(db *gorm.DB) *gorm.DB {
		return db.Where("title_id IN ?", titlesArrNative)
	}
	total := int64(0)
	db.Scopes(tiltesFilter, statusFilter).Model(&links).Count(&total)
	db.Scopes(database.Paginate(c), tiltesFilter, statusFilter).Find(&links)
	c.Header("X-Total-Count", strconv.FormatInt(total, 10))
	c.JSON(http.StatusOK, links)
}

func VerifyLink(c *gin.Context) {
	user, exists := c.Get(jwt.UserKey)
	if exists == false {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "user status wrong 1",
		})
		return
	}
	userInfo := user.(models.User)
	var db = database.GetDB()
	link := models.Link{}
	linkInDB := models.Link{}
	_ = c.ShouldBindJSON(&link)
	fmt.Println(link.Status)
	queryForTitle := db.First(&linkInDB, "id = ?", link.ID)
	if queryForTitle.RowsAffected <= 0 {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "link doesnt exist",
		})
		return
	}
	if linkInDB.Status != 0 {
		c.JSON(http.StatusOK, linkInDB)
		return
	}
	now := time.Now()
	result := db.Where("id = ?", link.ID).Updates(
		models.Link{DateVerified: &now, Status: link.Status, VerifiedById: &userInfo.ID, VerifiedBy: &userInfo},
	)
	if result.Error != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}
	db.First(&link, "id = ?", linkInDB.ID)
	c.JSON(http.StatusOK, link)
}

func AddLink(c *gin.Context) {
	var db = database.GetDB()
	link := models.Link{}
	title := models.Title{}
	_ = c.ShouldBindJSON(&link)
	if database.Cache != nil {
		titleExists, err := database.Cache.Exists("title_" + link.TitleID.String())
		if titleExists == false || err != nil {
			queryForTitle := db.First(&title, "id = ?", link.TitleID)
			if queryForTitle.RowsAffected <= 0 {
				c.JSON(http.StatusBadRequest, gin.H{
					"error": "title doesnt exist",
				})
				return
			}
			serialized, _ := json.Marshal(title)
			database.Cache.SetEx("title_"+link.TitleID.String(), serialized, 600)
		} else {
			titleSerialized, _ := database.Cache.Get("title_" + link.TitleID.String())
			err := json.Unmarshal(titleSerialized, &title)
			if err != nil {
				queryForTitle := db.First(&title, "id = ?", link.TitleID)
				if queryForTitle.RowsAffected <= 0 {
					c.JSON(http.StatusBadRequest, gin.H{
						"error": "title doesnt exist",
					})
					return
				}
			}
		}
	} else {
		queryForTitle := db.First(&title, "id = ?", link.TitleID)
		if queryForTitle.RowsAffected <= 0 {
			c.JSON(http.StatusBadRequest, gin.H{
				"error": "title doesnt exist",
			})
			return
		}
	}

	link.Title = &title
	link.TitleID = title.ID
	err := link.GenerateUniqueHash()
	if err != nil {
		c.AbortWithStatus(http.StatusBadRequest)
		return
	}

	linkHashSearch := db.First(&link, "hash = ?", link.UniqueHash)
	if linkHashSearch.RowsAffected > 0 {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "link already exists",
		})
		return
	}
	result := db.Omit("Title").Create(&link)
	if result.Error != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": result.Error,
		})
		return
	}
	c.JSON(http.StatusOK, link)
}
