package controller

import (
	"aptirsapi/database"
	"aptirsapi/middleware"
	"aptirsapi/models"
	"fmt"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"net/http"
	"regexp"
	"strconv"
	"strings"
)

var emailRegex = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

func CreateUserAuth(c *gin.Context) {
	var db = database.GetDB()
	auth := models.User{}
	createAuth := 0 // default value

	_ = c.ShouldBindJSON(&auth)

	if auth.Password != auth.ConfirmedPassword {
		createAuth = 3 // invalid email
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "auth.email_not_valid"})
		return
	}

	if isEmailValid(auth.Email) == false {
		createAuth = 1 // invalid email
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "auth.email_not_valid"})
		return
	}

	if err := db.Where("email = ?", auth.Email).First(&auth).Error; err == nil {
		createAuth = 2 // email is already registered
		c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "auth.email_already_exists"})
		return
	}

	// one unique email for each account
	if createAuth == 0 {
		tx := db.Begin()
		if err := tx.Create(&auth).Error; err != nil {
			tx.Rollback()
			fmt.Println(err)

			c.JSON(http.StatusBadRequest, gin.H{"success": false, "message": "auth.sign_up_error"})
		} else {
			tx.Commit()
			c.JSON(http.StatusCreated, gin.H{"success": true})
		}
	}
}

func isEmailValid(e string) bool {
	if len(e) < 3 && len(e) > 254 {
		return false
	}
	return emailRegex.MatchString(e)
}

func GetUser(c *gin.Context) {
	var db = database.GetDB()
	var user models.User
	userId := c.Param("user")
	db.Where("id = ?", models.ULIDParse(userId)).First(&user)
	c.JSON(http.StatusOK, user.ToDTO())
}

func GetUsers(c *gin.Context) {
	var db = database.GetDB()
	usertype := strings.ToLower(c.Query("type"))
	users := []models.User{}
	var filter func(db *gorm.DB) *gorm.DB
	switch usertype {
	case "active":
		filter = func(db *gorm.DB) *gorm.DB {
			return db.Not("role = ?", middleware.RoleUser)
		}
		break
	case "inactive":
		filter = func(db *gorm.DB) *gorm.DB {
			return db.Where("role = ?", middleware.RoleUser)
		}
		break
	default:
		filter = func(db *gorm.DB) *gorm.DB {
			return db
		}
		break
	}
	total := int64(0)
	db.Scopes(filter).Model(&users).Count(&total)
	db.Scopes(database.Paginate(c), filter).Find(&users)
	c.Header("X-Total-Count", strconv.FormatInt(total, 10))
	c.JSON(http.StatusOK, models.UserListToUserDTOList(users))
}

func GetRoles(c *gin.Context) {
	c.JSON(http.StatusOK, middleware.RolesList())
}

func UpdateUser(c *gin.Context) {
	var db = database.GetDB()
	var user models.User
	_ = c.ShouldBindJSON(&user)
	db.Model(&user).Updates(models.User{UserDTO: models.UserDTO{Name: user.Name, Email: user.Email, Role: user.Role}})
	c.JSON(http.StatusOK, user.ToDTO())
}

func RemoveUser(c *gin.Context) {
	var db = database.GetDB()
	var user models.User
	userId := c.Param("user")
	db.Where("id = ?", models.ULIDParse(userId)).First(&user)
	db.Delete(&user)
	c.JSON(http.StatusOK, user.ToDTO())
}
