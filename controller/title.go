package controller

import (
	"aptirsapi/database"
	"aptirsapi/models"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"net/http"
	"strconv"
)

func AddTitle(c *gin.Context) {
	var db = database.GetDB()
	title := models.Title{}
	_ = c.ShouldBindJSON(&title)
	if db.First(&title, "title_name = ? AND client_id = ?", title.TitleName, title.ClientID).RowsAffected > 0 {
		c.JSON(http.StatusConflict, gin.H{
			"error": "title already exist",
		})
		return
	}
	db.Create(&title)
	c.JSON(http.StatusCreated, title)
}

func GetTitle(c *gin.Context) {
	var db = database.GetDB()
	title := models.Title{}
	titleId := c.Param("title")
	if db.Where("id = ?", models.ULIDParse(titleId)).First(&title).RowsAffected <= 0 {
		c.JSON(http.StatusNotFound, gin.H{
			"error": "title doesnt exist",
		})
		return
	}
	c.JSON(http.StatusOK, title)
}

func RemoveTitle(c *gin.Context) {
	var db = database.GetDB()
	title := models.Title{}
	titleId := c.Param("title")
	if db.Where("id = ?", models.ULIDParse(titleId)).First(&title).RowsAffected <= 0 {
		c.JSON(http.StatusNotFound, gin.H{
			"error": "client doesnt exist",
		})
		return
	}
	db.Delete(&title)
	c.JSON(http.StatusOK, title)
}

func UpdateTitle(c *gin.Context) {
	var db = database.GetDB()
	title := models.Title{}
	_ = c.ShouldBindJSON(&title)
	if db.First(&title, "title_name = ? AND client_id = ?", title.TitleName, title.ClientID).RowsAffected > 0 {
		c.JSON(http.StatusConflict, gin.H{
			"error": "title already exist",
		})
		return
	}
	db.Model(&title).Updates(title)
	c.JSON(http.StatusOK, title)
}

func GetTitles(c *gin.Context, showAll bool) {
	var db = database.GetDB()
	titles := []models.Title{}
	client := c.Param("client")
	total := int64(0)
	db.Model(&titles).Count(&total)
	var filter func(db *gorm.DB) *gorm.DB
	switch showAll {
	case false:
		filter = database.Paginate(c)
		break
	default:
		filter = func(db *gorm.DB) *gorm.DB {
			return db
		}
		break
	}
	var filterClient func(db *gorm.DB) *gorm.DB
	if len(client) > 0 {
		filterClient = func(db *gorm.DB) *gorm.DB {
			return db.Where("client_id = ? ", models.ULIDParse(client))
		}
	} else {
		filterClient = func(db *gorm.DB) *gorm.DB {
			return db
		}
	}
	switch showAll {
	case false:
		filter = database.Paginate(c)
		break
	default:
		filter = func(db *gorm.DB) *gorm.DB {
			return db
		}
		break
	}
	db.Scopes(filter, filterClient).Find(&titles)
	c.Header("X-Total-Count", strconv.FormatInt(total, 10))
	c.JSON(http.StatusOK, titles)
}

func GetTitlesList(c *gin.Context) {
	var db = database.GetDB()
	var titles []models.Title
	db.Scopes().Find(&titles)
	c.JSON(http.StatusOK, titles)
}
