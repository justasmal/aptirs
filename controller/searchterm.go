package controller

import (
	"aptirsapi/database"
	"aptirsapi/models"
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
)

type SearchTermsListResponse struct {
	Id    string       `json:"id"`
	Title models.Title `json:"title"`
	Terms string       `json:"terms"`
}

type SearchTermsResponse struct {
	Id    string              `json:"id"`
	Title models.Title        `json:"title"`
	Terms []models.SearchTerm `json:"terms"`
}

type TermsJSONString struct {
	Terms string `json:"terms"`
}

func getTerms(title models.Title, c *gin.Context, isList bool) {
	var db = database.GetDB()
	var searchterms []models.SearchTerm
	db.Find(&searchterms).Where("titleid = ?", title.ID)
	if isList == true {
		response := SearchTermsResponse{
			Id:    title.ID.String(),
			Title: title,
			Terms: searchterms,
		}
		c.JSON(http.StatusOK, response)
	} else {
		termsStr := ""
		for i, searchterm := range searchterms {
			termsStr += searchterm.Term
			if i-1 != len(searchterms) {
				termsStr += "\r\n"
			}
		}
		response := SearchTermsListResponse{
			Id:    title.ID.String(),
			Title: title,
			Terms: termsStr,
		}
		c.JSON(http.StatusOK, response)
	}
}

func GetSearchTerms(c *gin.Context) {
	var db = database.GetDB()
	var title models.Title
	titleId := c.Param("titleid")
	islist := strings.ToLower(c.Param("islist"))
	result := db.First(&title, "id = ?", models.ULIDParse(titleId))
	if result.RowsAffected <= 0 {
		c.JSON(http.StatusNotFound, gin.H{
			"error": "title doesnt exist",
		})
		return
	}

	if strings.Contains(islist, "list") {
		getTerms(title, c, true)
	} else {
		getTerms(title, c, false)
	}
	return
}

func removeDuplicatesUnordered(elements []string) []string {
	encountered := map[string]bool{}

	// Create a map of all unique elements.
	for v := range elements {
		encountered[elements[v]] = true
	}

	// Place all keys from the map into a slice.
	var result []string
	for key, _ := range encountered {
		result = append(result, key)
	}
	return result
}

func UpdateSearchTerms(c *gin.Context) {
	var db = database.GetDB()
	var title models.Title
	var searchterms []models.SearchTerm
	titleId := c.Param("titleid")
	result := db.First(&title, "id = ?", models.ULIDParse(titleId))
	if result.RowsAffected <= 0 {
		c.JSON(http.StatusNotFound, gin.H{
			"error": "title doesnt exist",
		})
		return
	}
	termsStr := c.Request.PostForm.Get("terms")
	if len(termsStr) <= 0 {
		termsJSON := TermsJSONString{}
		_ = c.ShouldBindJSON(&termsJSON)
		termsStr = termsJSON.Terms
	}
	termsArr := removeDuplicatesUnordered(strings.Fields(termsStr))
	db.Where("title_id = ?", title.ID).Delete(models.SearchTerm{})
	for _, searchterm := range termsArr {
		searchterms = append(searchterms, models.SearchTerm{
			Term:    searchterm,
			TitleID: title.ID,
		})

	}
	db.Create(&searchterms)
	getTerms(title, c, false)
}
