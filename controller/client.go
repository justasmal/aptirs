package controller

import (
	"aptirsapi/database"
	"aptirsapi/models"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

func AddClient(c *gin.Context) {
	var db = database.GetDB()
	client := models.Client{}
	_ = c.ShouldBindJSON(&client)
	if db.First(&client, "name = ?", client.Name).RowsAffected > 0 {
		c.JSON(http.StatusConflict, gin.H{
			"error": "client already exist",
		})
		return
	}
	db.Create(&client)
	c.JSON(http.StatusCreated, client)
}

func GetClient(c *gin.Context) {
	var db = database.GetDB()
	client := models.Client{}
	clientId := c.Param("client")
	if db.Where("id = ?", models.ULIDParse(clientId)).First(&client).RowsAffected <= 0 {
		c.JSON(http.StatusNotFound, gin.H{
			"error": "client doesnt exist",
		})
		return
	}
	c.JSON(http.StatusOK, client)
}

func RemoveClient(c *gin.Context) {
	var db = database.GetDB()
	client := models.Client{}
	clientId := c.Param("client")
	if db.Where("id = ?", models.ULIDParse(clientId)).First(&client).RowsAffected <= 0 {
		c.JSON(http.StatusNotFound, gin.H{
			"error": "client doesnt exist",
		})
		return
	}
	db.Delete(&client)
	c.JSON(http.StatusOK, client)
}

func UpdateClient(c *gin.Context) {
	var db = database.GetDB()
	client := models.Client{}
	_ = c.ShouldBindJSON(&client)
	if db.First(&client, "id = ?", client.ID).RowsAffected <= 0 {
		c.JSON(http.StatusNotFound, gin.H{
			"error": "client doesnt exist",
		})
		return
	}
	db.Model(&client).Updates(client)
	c.JSON(http.StatusOK, client)
}

func GetClients(c *gin.Context) {
	var db = database.GetDB()
	var clients []models.Client
	total := int64(0)
	db.Model(&clients).Count(&total)
	db.Scopes(database.Paginate(c)).Find(&clients)
	c.Header("X-Total-Count", strconv.FormatInt(total, 10))
	c.JSON(http.StatusOK, models.ClientListToToSimpleDTOList(clients))

}

func GetClientsList(c *gin.Context) {
	var db = database.GetDB()
	var clients []models.Client
	db.Scopes().Find(&clients)
	c.JSON(http.StatusOK, models.ClientListToToSimpleDTOList(clients))
}
