package controller

import (
	"aptirsapi/database"
	"aptirsapi/dto"
	"aptirsapi/middleware"
	"aptirsapi/models"
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/oklog/ulid/v2"
	"github.com/tealeg/xlsx/v3"
	"gorm.io/gorm"
	"net/http"
	"strconv"
	"strings"
	"time"
)

func GetReportFields(c *gin.Context) {
	fields := []map[string]string{}
	for _, v := range dto.ReportFields {
		fields = append(fields, v.GetForWebResponse())
	}
	c.JSON(http.StatusOK, fields)
}

func GetReports(c *gin.Context) {
	var db = database.GetDB()
	reports := []models.Report{}
	client, err := models.ULIDStrictParse(c.Param("client"))
	total := int64(0)
	totalChain := db.Model(&reports)
	if err == nil {
		totalChain = totalChain.Where("client_id = ? ", client).Count(&total)
	} else {
		totalChain = totalChain.Count(&total)
	}
	chain := db.Scopes(database.Paginate(c)).Find(&reports).Preload("Client")
	if err == nil {
		chain = chain.Where("client_id = ? ", client)
	}
	c.Header("X-Total-Count", strconv.FormatInt(total, 10))
	c.JSON(http.StatusOK, reports)
}

func AddReport(c *gin.Context) {
	var db = database.GetDB()
	var reportField = dto.ReportField{}
	report := models.Report{}
	_ = c.ShouldBindJSON(&report)
	fields := report.Fields
	fieldVar := []string{}
	_ = json.Unmarshal(fields, &fieldVar)
	newFields := []string{}
	for _, field := range fieldVar {
		reportField.GetFromWebResponseString(field)
		newFields = append(newFields, reportField.Value)
	}
	report.Fields, _ = json.Marshal(newFields)
	if db.First(&report, "AND name = ?", report.Name).RowsAffected > 0 {
		c.JSON(http.StatusConflict, gin.H{
			"error": "report with such name already exist",
		})
		return
	}
	db.Create(&report)
	c.JSON(http.StatusCreated, report)
}

func RemoveReport(c *gin.Context) {
	var db = database.GetDB()
	report := models.Report{}
	reportId := c.Param("report")
	if db.Where("id = ?", models.ULIDParse(reportId)).First(&report).RowsAffected <= 0 {
		c.JSON(http.StatusNotFound, gin.H{
			"error": "report doesnt exist",
		})
		return
	}
	db.Delete(&reportId)
	c.JSON(http.StatusOK, reportId)
}

func GetReport(c *gin.Context) {
	var db = database.GetDB()
	report := models.Report{}
	reportId := c.Param("report")
	if db.Where("id = ?", models.ULIDParse(reportId)).First(&report).RowsAffected <= 0 {
		c.JSON(http.StatusNotFound, gin.H{
			"error": "report doesnt exist",
		})
		return
	}
	fields := []string{}
	reportFields := []string{}
	reportField := dto.ReportField{}
	_ = report.Fields.UnmarshalJSON(report.Fields)
	_ = json.Unmarshal(report.Fields, &fields)
	for _, field := range fields {
		reportField.GetReportResponseFromValue(field)
		reportFields = append(reportFields, reportField.Field)
	}
	report.Fields, _ = json.Marshal(reportFields)
	c.JSON(http.StatusOK, report)
}

func GenExportToken(c *gin.Context) {
	reportId := c.Param("report")
	format := "2006-01-02 15:04:05"
	from, _ := time.Parse(format, c.Query("from")+" 00:00:00")
	to, _ := time.Parse(format, c.Query("to")+" 23:59:59")
	linkStatus, _ := strconv.Atoi(c.Query("link_status"))
	fileType := strings.ToLower(c.Query("type"))
	exportData, err := json.Marshal(models.ExportData{
		ReportID:   reportId,
		From:       from,
		To:         to,
		LinkStatus: linkStatus,
		FileType:   fileType,
	})
	token, err := middleware.GenTempJWT(60*15, string(exportData))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
	}
	c.JSON(http.StatusOK, gin.H{
		"token": token,
	})
}

func ParseToken(token string) (models.ExportData, error) {
	var data models.ExportData
	dec, err := middleware.ValidateTempJWT(token)
	if err != nil {
		return data, err
	}
	str := fmt.Sprintf("%v", dec)
	err = json.Unmarshal([]byte(str), &data)
	if err != nil {
		return data, err
	}
	return data, nil
}

func GetFieldVal(link models.Link, field string) string {
	switch field {
	case "clientname":
		return link.Title.Client.Name
	case "titlename":
		return link.Title.TitleName
	case "url":
		return link.URL
	case "linkingurl":
		return link.LinkingURL
	case "status":
		status := "Non Verified"
		switch link.Status {
		case 1:
			status = "Pirated Link"
			break
		case 2:
			status = "Non Pirated Link"
		}
		return status
	case "datefound":
		return link.DateFound.Format("2006-01-02 15:04:05")
	case "dateverified":
		return link.DateVerified.Format("2006-01-02 15:04:05")
	case "description":
		return link.Description
	case "username":
		return link.VerifiedBy.Name
	default:
		return ""
	}
}

func DoExport(c *gin.Context) {
	token := c.Query("token")
	data, err := ParseToken(token)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "error",
		})
		return
	}
	var db = database.GetDB()
	report := models.Report{}
	links := []models.Link{}
	if db.Where("id = ?", ulid.MustParse(data.ReportID)).First(&report).RowsAffected <= 0 {
		c.JSON(http.StatusNotFound, gin.H{
			"error": "report doesnt exist",
		})
		return
	}
	var filter = func(db *gorm.DB) *gorm.DB {
		return db
	}

	if data.LinkStatus == 1 || data.LinkStatus == 2 {
		filter = func(db *gorm.DB) *gorm.DB {
			return db.Where("status = ?", data.LinkStatus)
		}
	}
	db.Scopes(filter).Find(&links).Where("date_verified >= ? AND date_verified <= ?", data.From, data.To)
	fields := []string{}
	_ = report.Fields.UnmarshalJSON(report.Fields)
	_ = json.Unmarshal(report.Fields, &fields)
	switch data.FileType {
	case "xlsx":
	default:
		break

	}

	rf := dto.ReportField{}
	file := xlsx.NewFile()
	sheet, err := file.AddSheet("Sheet 1")
	if err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	rightStyle := xlsx.NewStyle()
	rightStyle.Alignment.Horizontal = "center"
	rightStyle.ApplyAlignment = true
	row := sheet.AddRow()
	row.SetHeight(12.85)
	for _, field := range fields {
		cell := row.AddCell()
		cell.SetStyle(rightStyle)
		cell.Value = rf.GetFieldName(field)
	}

	for _, link := range links {
		rowLoop := sheet.AddRow()
		rowLoop.SetHeight(12.85)
		for _, field := range fields {
			cell := rowLoop.AddCell()
			cell.Value = GetFieldVal(link, field)
		}
	}
	var b bytes.Buffer
	if err := file.Write(&b); err != nil {
		c.JSON(http.StatusInternalServerError, err.Error())
		return
	}
	downloadName := time.Now().UTC().Format("export-20060102150405.xlsx")
	c.Header("Content-Description", "File Transfer")
	c.Header("Content-Disposition", "attachment; filename="+downloadName)
	c.Data(http.StatusOK, "application/octet-stream", b.Bytes())
}
