package controller

import (
	"aptirsapi/database"
	"github.com/gin-gonic/gin"
)

func HealthCheck(c *gin.Context) {
	sqlDB, err := database.GetDB().DB()
	if err != nil {
		c.AbortWithStatus(500)
		return
	}
	err = sqlDB.Ping()
	if err != nil {
		c.AbortWithStatus(500)
		return
	}
	c.Status(200)
}
