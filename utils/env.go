package utils

import (
	"errors"
	"os"
)

func GetEnv(key string) (string, error) {
	val := os.Getenv(key)
	if len(val) == 0 {
		return "", errors.New("env value is empty")
	}
	return val, nil
}
