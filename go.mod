module aptirsapi

go 1.16

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-gonic/gin v1.7.1
	github.com/go-redis/redis/v8 v8.8.2 // indirect
	github.com/go-sql-driver/mysql v1.6.0 // indirect
	github.com/gomodule/redigo v2.0.0+incompatible // indirect
	github.com/jackc/pgx/v4 v4.10.1 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/kyfk/gin-jwt v0.0.0-20191024073357-fd8387d8d220
	github.com/oklog/ulid/v2 v2.0.2
	github.com/tealeg/xlsx/v3 v3.2.3
	golang.org/x/crypto v0.0.0-20210322153248-0c34fe9e7dc2
	gorm.io/datatypes v1.0.1
	gorm.io/driver/mysql v1.0.5
	gorm.io/driver/postgres v1.0.8 // indirect
	gorm.io/gorm v1.21.7
	gorm.io/plugin/dbresolver v1.1.0
)

replace github.com/kyfk/gin-jwt => gitlab.com/justasmal/gin-jwt v0.0.0-20210410224423-eb8cd3c50b82
