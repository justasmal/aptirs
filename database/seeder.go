package database

import (
	"aptirsapi/models"
	"fmt"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"strconv"
)

func SeedAll(db *gorm.DB) {
	SeedUsers(db)
	SeedClients(db)
	SeedTitles(db)
}

func SeedUsers(db *gorm.DB) {
	users := []models.User{}
	users = append(users, models.User{
		UserDTO: models.UserDTO{
			ID:    models.ULIDParse("01F33DZYTZ29P49PRXR76PTHAD"),
			Email: "useraccount@outlook.com",
			Role:  "ROLE_ADMIN",
			Name:  "Justas Malinauskas",
		},
		Password: "admin",
	})
	users = append(users, models.User{
		UserDTO: models.UserDTO{
			ID:    models.ULIDParse("01F3E5G906AGVTNCD0WXZRJ5VG"),
			Email: "admin@aptirs.lt",
			Role:  "ROLE_ADMIN",
			Name:  "Adminstratorius",
		},
		Password: "admin",
	})
	users = append(users, models.User{
		UserDTO: models.UserDTO{
			ID:    models.ULIDParse("01F3E5GYKTTD4P08C2PPMC1RG2"),
			Email: "vadovas@aptirs.lt",
			Role:  "ROLE_MANAGER",
			Name:  "Vadovas",
		},
		Password: "manager",
	})
	users = append(users, models.User{
		UserDTO: models.UserDTO{
			ID:    models.ULIDParse("01F3E5HZV1Y8ZAWCFQHZ3DTHQP"),
			Email: "analitikas@aptirs.lt",
			Role:  "ROLE_ANALYST",
			Name:  "Analitikas",
		},
		Password: "analyst",
	})
	dbQuery := db.Clauses(clause.OnConflict{Columns: []clause.Column{{Name: "id"}}, DoNothing: true}).Save(&users)
	fmt.Println("Users added: " + strconv.FormatInt(dbQuery.RowsAffected, 10))
}

func SeedClients(db *gorm.DB) {
	clients := []models.Client{}
	clients = append(clients, models.Client{
		ClientSimpleDTO: models.ClientSimpleDTO{
			ID:   models.ULIDParse("01F33FMPZDJTJM2HQ0XSYC9MT3"),
			Name: "Klientas 1",
		},
	})
	clients = append(clients, models.Client{
		ClientSimpleDTO: models.ClientSimpleDTO{
			ID:   models.ULIDParse("01F3DXPWS3F59R0SZMJ9ME0GEH"),
			Name: "Klientas 2",
		},
	})
	clients = append(clients, models.Client{
		ClientSimpleDTO: models.ClientSimpleDTO{
			ID:   models.ULIDParse("01F3DXQ4JGVKGAXSRBKDA1BKDE"),
			Name: "Klientas 3",
		},
	})
	clients = append(clients, models.Client{
		ClientSimpleDTO: models.ClientSimpleDTO{
			ID:   models.ULIDParse("01F3DXQBJAE720SSQ93RCGXZSV"),
			Name: "Klientas 4",
		},
	})
	clients = append(clients, models.Client{
		ClientSimpleDTO: models.ClientSimpleDTO{
			ID:   models.ULIDParse("01F3DXQKBSH76CY9CX7EPWPS85"),
			Name: "Klientas 5",
		},
	})
	clients = append(clients, models.Client{
		ClientSimpleDTO: models.ClientSimpleDTO{
			ID:   models.ULIDParse("01F3DXQSRM5E8SJVGBJPKNZ2JP"),
			Name: "Klientas 6",
		},
	})
	clients = append(clients, models.Client{
		ClientSimpleDTO: models.ClientSimpleDTO{
			ID:   models.ULIDParse("01F3DXR0B2PNW1FRR9JZR9VBC3"),
			Name: "Klientas 7",
		},
	})
	clients = append(clients, models.Client{
		ClientSimpleDTO: models.ClientSimpleDTO{
			ID:   models.ULIDParse("01F3DXR7ZAFRKN7B56TRQ4DJWV"),
			Name: "Klientas 8",
		},
	})
	clients = append(clients, models.Client{
		ClientSimpleDTO: models.ClientSimpleDTO{
			ID:   models.ULIDParse("01F3DXRGG9SZ2Y1TXBJ3Y1A6CR"),
			Name: "Klientas 9",
		},
	})
	clients = append(clients, models.Client{
		ClientSimpleDTO: models.ClientSimpleDTO{
			ID:   models.ULIDParse("01F3DXRS0PNV978CZC2BFZYXWA"),
			Name: "Klientas 10",
		},
	})
	dbQuery := db.Clauses(clause.OnConflict{DoNothing: true}).Create(&clients)
	fmt.Println("Clients added: " + strconv.FormatInt(dbQuery.RowsAffected, 10))
}

func SeedTitles(db *gorm.DB) {
	titles := []models.Title{}
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0HRWHD26662HAR0NP2Y22"), TitleName: "Title 1", ClientID: models.ULIDParse("01F33FMPZDJTJM2HQ0XSYC9MT3")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0HXPJFX7GGQ6215SA8Q5X"), TitleName: "Title 2", ClientID: models.ULIDParse("01F33FMPZDJTJM2HQ0XSYC9MT3")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0J2F0JRQEJ445PJQV30HA"), TitleName: "Title 3", ClientID: models.ULIDParse("01F33FMPZDJTJM2HQ0XSYC9MT3")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0J4Y38THGJFFYH2K22GNJ"), TitleName: "Title 4", ClientID: models.ULIDParse("01F33FMPZDJTJM2HQ0XSYC9MT3")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0J7GW4FA971MQJ59TEGQS"), TitleName: "Title 5", ClientID: models.ULIDParse("01F33FMPZDJTJM2HQ0XSYC9MT3")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0JA2Q1Z266PC56W2G34FT"), TitleName: "Title 6", ClientID: models.ULIDParse("01F33FMPZDJTJM2HQ0XSYC9MT3")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0JCB0G64TRFSYSMNJQTEE"), TitleName: "Title 7", ClientID: models.ULIDParse("01F33FMPZDJTJM2HQ0XSYC9MT3")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0JEPX134X84EPB0F2B8MG"), TitleName: "Title 8", ClientID: models.ULIDParse("01F33FMPZDJTJM2HQ0XSYC9MT3")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0JGP8Y8Q35K6F3JQM9B4Q"), TitleName: "Title 9", ClientID: models.ULIDParse("01F33FMPZDJTJM2HQ0XSYC9MT3")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0JJRHNQ91WBQCTQ08B43N"), TitleName: "Title 10", ClientID: models.ULIDParse("01F33FMPZDJTJM2HQ0XSYC9MT3")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0K5XEXV3S5AG09DH3FY90"), TitleName: "Title 11", ClientID: models.ULIDParse("01F3DXPWS3F59R0SZMJ9ME0GEH")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0K84ZDN4D3Z7QS6TZJWX7"), TitleName: "Title 12", ClientID: models.ULIDParse("01F3DXPWS3F59R0SZMJ9ME0GEH")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0K9T776FYR03ZGXRQ0TBZ"), TitleName: "Title 13", ClientID: models.ULIDParse("01F3DXPWS3F59R0SZMJ9ME0GEH")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0KBFD0W9HMW2NNMACEG9W"), TitleName: "Title 14", ClientID: models.ULIDParse("01F3DXPWS3F59R0SZMJ9ME0GEH")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0KD0X3NDA2ACYK7ZJ6Z5E"), TitleName: "Title 15", ClientID: models.ULIDParse("01F3DXPWS3F59R0SZMJ9ME0GEH")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0KEMH44TGNAAEKAZPJP5M"), TitleName: "Title 16", ClientID: models.ULIDParse("01F3DXPWS3F59R0SZMJ9ME0GEH")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0KG5WR8V6S53VTNSHWQMH"), TitleName: "Title 17", ClientID: models.ULIDParse("01F3DXPWS3F59R0SZMJ9ME0GEH")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0KHQM3S10KRZ4HJ8AKEK1"), TitleName: "Title 18", ClientID: models.ULIDParse("01F3DXPWS3F59R0SZMJ9ME0GEH")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0KK8RYHX9WMF09824HQWE"), TitleName: "Title 19", ClientID: models.ULIDParse("01F3DXPWS3F59R0SZMJ9ME0GEH")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0KN4P8R4A875DT3Z1G3KC"), TitleName: "Title 20", ClientID: models.ULIDParse("01F3DXPWS3F59R0SZMJ9ME0GEH")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0KWP866F0BKQP8RYDCPK5"), TitleName: "Title 21", ClientID: models.ULIDParse("01F3DXQ4JGVKGAXSRBKDA1BKDE")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0KYAMFK6NTVJQPRZQW9X9"), TitleName: "Title 22", ClientID: models.ULIDParse("01F3DXQ4JGVKGAXSRBKDA1BKDE")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0KZXNKTPZ80VDKW0RQWND"), TitleName: "Title 23", ClientID: models.ULIDParse("01F3DXQ4JGVKGAXSRBKDA1BKDE")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0M1FW0F9YRFVJCPFB2AXW"), TitleName: "Title 24", ClientID: models.ULIDParse("01F3DXQ4JGVKGAXSRBKDA1BKDE")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0M30BX3MV83DS6KQT6VQA"), TitleName: "Title 25", ClientID: models.ULIDParse("01F3DXQ4JGVKGAXSRBKDA1BKDE")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0M4H0924TC2T5DXWYXFYH"), TitleName: "Title 26", ClientID: models.ULIDParse("01F3DXQ4JGVKGAXSRBKDA1BKDE")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0M5YEJQWA28TM1XPV8PAE"), TitleName: "Title 27", ClientID: models.ULIDParse("01F3DXQ4JGVKGAXSRBKDA1BKDE")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0M7CQYWDJ0JCRTT3K0QD7"), TitleName: "Title 28", ClientID: models.ULIDParse("01F3DXQ4JGVKGAXSRBKDA1BKDE")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0M8V8JP0NQ3VQSH6DJ9WT"), TitleName: "Title 29", ClientID: models.ULIDParse("01F3DXQ4JGVKGAXSRBKDA1BKDE")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0MAYJA1PQPQTE0HJ7N930"), TitleName: "Title 30", ClientID: models.ULIDParse("01F3DXQ4JGVKGAXSRBKDA1BKDE")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0MJ779VRCPX61A7T73S2A"), TitleName: "Title 31", ClientID: models.ULIDParse("01F3DXQBJAE720SSQ93RCGXZSV")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0MM631B1MG1EQJXV6R6B3"), TitleName: "Title 32", ClientID: models.ULIDParse("01F3DXQBJAE720SSQ93RCGXZSV")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0MNPACNDSTVW54FYDQB43"), TitleName: "Title 33", ClientID: models.ULIDParse("01F3DXQBJAE720SSQ93RCGXZSV")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0MQ8XD4AK8NH4TWRKEJRJ"), TitleName: "Title 34", ClientID: models.ULIDParse("01F3DXQBJAE720SSQ93RCGXZSV")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0MRPHE4PZTGQ87RK2C5QB"), TitleName: "Title 35", ClientID: models.ULIDParse("01F3DXQBJAE720SSQ93RCGXZSV")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0MT6WNC4T05J49XA2XTYA"), TitleName: "Title 36", ClientID: models.ULIDParse("01F3DXQBJAE720SSQ93RCGXZSV")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0MVMN0QG1RKSTM6W2YEAJ"), TitleName: "Title 37", ClientID: models.ULIDParse("01F3DXQBJAE720SSQ93RCGXZSV")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0MX4E5QCVZT23ETCPQWD0"), TitleName: "Title 38", ClientID: models.ULIDParse("01F3DXQBJAE720SSQ93RCGXZSV")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0MYMG7NNVJNG1HAB27WSE"), TitleName: "Title 39", ClientID: models.ULIDParse("01F3DXQBJAE720SSQ93RCGXZSV")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0N0WKB2GEGG11GJD1D5GR"), TitleName: "Title 40", ClientID: models.ULIDParse("01F3DXQBJAE720SSQ93RCGXZSV")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0N7Z0EX17BBV914M79HGA"), TitleName: "Title 41", ClientID: models.ULIDParse("01F3DXQKBSH76CY9CX7EPWPS85")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0N9JSM4HA05KVBD8XNT4K"), TitleName: "Title 42", ClientID: models.ULIDParse("01F3DXQKBSH76CY9CX7EPWPS85")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0NB35XJ6J475M49S259KJ"), TitleName: "Title 43", ClientID: models.ULIDParse("01F3DXQKBSH76CY9CX7EPWPS85")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0NCKZCZ9YPZKVWNKNDBEF"), TitleName: "Title 44", ClientID: models.ULIDParse("01F3DXQKBSH76CY9CX7EPWPS85")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0NE5WSFNQ9PP3A51QRQTC"), TitleName: "Title 45", ClientID: models.ULIDParse("01F3DXQKBSH76CY9CX7EPWPS85")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0NFX4RD9Q1HMMRQKE372P"), TitleName: "Title 46", ClientID: models.ULIDParse("01F3DXQKBSH76CY9CX7EPWPS85")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0NJHS1PJPVJ6R1WF9YG2T"), TitleName: "Title 47", ClientID: models.ULIDParse("01F3DXQKBSH76CY9CX7EPWPS85")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0NMEP3SVMRN824JN601H0"), TitleName: "Title 48", ClientID: models.ULIDParse("01F3DXQKBSH76CY9CX7EPWPS85")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0NNVW7BE03129ZM08WBM8"), TitleName: "Title 49", ClientID: models.ULIDParse("01F3DXQKBSH76CY9CX7EPWPS85")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0NQTBE075CE5FJFRJXW8X"), TitleName: "Title 50", ClientID: models.ULIDParse("01F3DXQKBSH76CY9CX7EPWPS85")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0NYDNJ96HJJ6CKHJXQRCY"), TitleName: "Title 51", ClientID: models.ULIDParse("01F3DXQSRM5E8SJVGBJPKNZ2JP")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0P02QE8135ZQZMMTG2QE4"), TitleName: "Title 52", ClientID: models.ULIDParse("01F3DXQSRM5E8SJVGBJPKNZ2JP")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0P1K98K198K7KTNK779NY"), TitleName: "Title 53", ClientID: models.ULIDParse("01F3DXQSRM5E8SJVGBJPKNZ2JP")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0P2ZWQW7ZKG44G4N92N9T"), TitleName: "Title 54", ClientID: models.ULIDParse("01F3DXQSRM5E8SJVGBJPKNZ2JP")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0P4CB24WX3H10JBQ1GH1Y"), TitleName: "Title 55", ClientID: models.ULIDParse("01F3DXQSRM5E8SJVGBJPKNZ2JP")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0P5TA0V3J4FV4686969QN"), TitleName: "Title 56", ClientID: models.ULIDParse("01F3DXQSRM5E8SJVGBJPKNZ2JP")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0P7ET586BMT3XZ13Y8N29"), TitleName: "Title 57", ClientID: models.ULIDParse("01F3DXQSRM5E8SJVGBJPKNZ2JP")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0P93RDWF8ME6PB6H8C9G7"), TitleName: "Title 58", ClientID: models.ULIDParse("01F3DXQSRM5E8SJVGBJPKNZ2JP")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0PAN3N3TG08T521F2VMGK"), TitleName: "Title 59", ClientID: models.ULIDParse("01F3DXQSRM5E8SJVGBJPKNZ2JP")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0PCEY7HYM0G9TX9P68RD3"), TitleName: "Title 60", ClientID: models.ULIDParse("01F3DXQSRM5E8SJVGBJPKNZ2JP")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0PJHX8E7TM3RSSXG1E7PH"), TitleName: "Title 61", ClientID: models.ULIDParse("01F3DXR0B2PNW1FRR9JZR9VBC3")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0PM61SYZQ1EX2NKK9KK1Z"), TitleName: "Title 62", ClientID: models.ULIDParse("01F3DXR0B2PNW1FRR9JZR9VBC3")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0PP665GHYSEGSGBV4MHFZ"), TitleName: "Title 63", ClientID: models.ULIDParse("01F3DXR0B2PNW1FRR9JZR9VBC3")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0PQRBM6490SA5KW94BQDT"), TitleName: "Title 64", ClientID: models.ULIDParse("01F3DXR0B2PNW1FRR9JZR9VBC3")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0PS9ACRQFR7CWHJP06QCR"), TitleName: "Title 65", ClientID: models.ULIDParse("01F3DXR0B2PNW1FRR9JZR9VBC3")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0PWSVMEA8FRYD14Y5M58Q"), TitleName: "Title 66", ClientID: models.ULIDParse("01F3DXR0B2PNW1FRR9JZR9VBC3")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0PYHYE3ZCXWNK6FKDCB95"), TitleName: "Title 67", ClientID: models.ULIDParse("01F3DXR0B2PNW1FRR9JZR9VBC3")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0PZXC5VK6RA5XT7PNA7NZ"), TitleName: "Title 68", ClientID: models.ULIDParse("01F3DXR0B2PNW1FRR9JZR9VBC3")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0Q1P874F2Z693Z9ERPP44"), TitleName: "Title 69", ClientID: models.ULIDParse("01F3DXR0B2PNW1FRR9JZR9VBC3")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0Q3E69G9ZNXZTB6P4EXXF"), TitleName: "Title 70", ClientID: models.ULIDParse("01F3DXR0B2PNW1FRR9JZR9VBC3")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0Q55JGFP9ACXCBKAYJFD5"), TitleName: "Title 71", ClientID: models.ULIDParse("01F3DXR0B2PNW1FRR9JZR9VBC3")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0Q6HJHTX9JQ0VCKDR85T1"), TitleName: "Title 72", ClientID: models.ULIDParse("01F3DXR0B2PNW1FRR9JZR9VBC3")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0Q82Y9D8GDJPZW4AQV0WB"), TitleName: "Title 73", ClientID: models.ULIDParse("01F3DXR0B2PNW1FRR9JZR9VBC3")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0Q9ED7YZ3CVJH2ZPDMJBT"), TitleName: "Title 74", ClientID: models.ULIDParse("01F3DXR0B2PNW1FRR9JZR9VBC3")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0QAY63Q7R524QJ5RYWPY2"), TitleName: "Title 75", ClientID: models.ULIDParse("01F3DXR0B2PNW1FRR9JZR9VBC3")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0QCB884WDEMS0F50A0MFY"), TitleName: "Title 76", ClientID: models.ULIDParse("01F3DXR0B2PNW1FRR9JZR9VBC3")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0QDRHPDMTQ4QT365PYA7Q"), TitleName: "Title 77", ClientID: models.ULIDParse("01F3DXR0B2PNW1FRR9JZR9VBC3")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0QF94BFAE708Q6K6NXZ4S"), TitleName: "Title 78", ClientID: models.ULIDParse("01F3DXR0B2PNW1FRR9JZR9VBC3")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0QGPH9M65C6WXEYTDP8BS"), TitleName: "Title 79", ClientID: models.ULIDParse("01F3DXR0B2PNW1FRR9JZR9VBC3")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0QJB8DGF43ZNP3X79SSS8"), TitleName: "Title 80", ClientID: models.ULIDParse("01F3DXR0B2PNW1FRR9JZR9VBC3")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0R3YA3XRFVRYMXXWCZWQR"), TitleName: "Title 81", ClientID: models.ULIDParse("01F3DXR7ZAFRKN7B56TRQ4DJWV")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0R5J3H3BBR7FJ0CSQG38M"), TitleName: "Title 82", ClientID: models.ULIDParse("01F3DXR7ZAFRKN7B56TRQ4DJWV")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0R6XMGFJ3N0HGTYKKQBYE"), TitleName: "Title 83", ClientID: models.ULIDParse("01F3DXR7ZAFRKN7B56TRQ4DJWV")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0R88QW2EH9JWNFNGNGNC3"), TitleName: "Title 84", ClientID: models.ULIDParse("01F3DXR7ZAFRKN7B56TRQ4DJWV")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0RA96A6AB9CD50768N0NW"), TitleName: "Title 85", ClientID: models.ULIDParse("01F3DXR7ZAFRKN7B56TRQ4DJWV")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0RBJFD6DB5DY61VGZW4WB"), TitleName: "Title 86", ClientID: models.ULIDParse("01F3DXR7ZAFRKN7B56TRQ4DJWV")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0RDFWXJ4BEJH06J47MH1C"), TitleName: "Title 87", ClientID: models.ULIDParse("01F3DXR7ZAFRKN7B56TRQ4DJWV")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0RF0ANK5DY8K1ENPQ1E68"), TitleName: "Title 88", ClientID: models.ULIDParse("01F3DXR7ZAFRKN7B56TRQ4DJWV")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0RHDW5KA41ZNHKGJGMXV6"), TitleName: "Title 89", ClientID: models.ULIDParse("01F3DXR7ZAFRKN7B56TRQ4DJWV")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0RMPH2H42DGKB3G8JRZ8N"), TitleName: "Title 90", ClientID: models.ULIDParse("01F3DXR7ZAFRKN7B56TRQ4DJWV")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0RXYX0M2BDBVWA15KX7J6"), TitleName: "Title 91", ClientID: models.ULIDParse("01F3DXRGG9SZ2Y1TXBJ3Y1A6CR")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0RZHSKSTBCGKAXJDGCRAG"), TitleName: "Title 92", ClientID: models.ULIDParse("01F3DXRGG9SZ2Y1TXBJ3Y1A6CR")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0S0RXD0G187DFHZSYFY2G"), TitleName: "Title 93", ClientID: models.ULIDParse("01F3DXRGG9SZ2Y1TXBJ3Y1A6CR")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0S233NE9WB8Q555850S2K"), TitleName: "Title 94", ClientID: models.ULIDParse("01F3DXRGG9SZ2Y1TXBJ3Y1A6CR")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0S3FNYPYK7687S7BHX9K1"), TitleName: "Title 95", ClientID: models.ULIDParse("01F3DXRGG9SZ2Y1TXBJ3Y1A6CR")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0S4Y8MVR23KVH5K91B7PZ"), TitleName: "Title 96", ClientID: models.ULIDParse("01F3DXRGG9SZ2Y1TXBJ3Y1A6CR")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0S6TAC2CGTDC9JFW9CRCM"), TitleName: "Title 97", ClientID: models.ULIDParse("01F3DXRGG9SZ2Y1TXBJ3Y1A6CR")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0S88HP51G2C65PX6QBF9P"), TitleName: "Title 98", ClientID: models.ULIDParse("01F3DXRGG9SZ2Y1TXBJ3Y1A6CR")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0S9NCTT6Q922TX2G0DTM8"), TitleName: "Title 99", ClientID: models.ULIDParse("01F3DXRGG9SZ2Y1TXBJ3Y1A6CR")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0SBR8WW9KFEB4G1WE5G3A"), TitleName: "Title 100", ClientID: models.ULIDParse("01F3DXRGG9SZ2Y1TXBJ3Y1A6CR")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0TND5AQBY9VDT6G02QFHQ"), TitleName: "Title 101", ClientID: models.ULIDParse("01F3DXRS0PNV978CZC2BFZYXWA")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0TQ8P18R10320JWTK1B8H"), TitleName: "Title 102", ClientID: models.ULIDParse("01F3DXRS0PNV978CZC2BFZYXWA")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0TRHE3RQP59Z9AKWB51B5"), TitleName: "Title 103", ClientID: models.ULIDParse("01F3DXRS0PNV978CZC2BFZYXWA")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0TSP6030EQ9FBMSNTNSQN"), TitleName: "Title 104", ClientID: models.ULIDParse("01F3DXRS0PNV978CZC2BFZYXWA")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0TV0QTF9CJR6AG2NJ4CYJ"), TitleName: "Title 105", ClientID: models.ULIDParse("01F3DXRS0PNV978CZC2BFZYXWA")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0TW7NJWW2CRBK38C1WHD4"), TitleName: "Title 106", ClientID: models.ULIDParse("01F3DXRS0PNV978CZC2BFZYXWA")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0TXDTEY0XQF45STX90KNC"), TitleName: "Title 107", ClientID: models.ULIDParse("01F3DXRS0PNV978CZC2BFZYXWA")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0TYPFXWFG5QM2YD12XC3Z"), TitleName: "Title 108", ClientID: models.ULIDParse("01F3DXRS0PNV978CZC2BFZYXWA")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0V0276Q9CYHG71RP16F73"), TitleName: "Title 109", ClientID: models.ULIDParse("01F3DXRS0PNV978CZC2BFZYXWA")})
	titles = append(titles, models.Title{ID: models.ULIDParse("01F3E0V25VSTYKQ7AZATDX0ED1"), TitleName: "Title 110", ClientID: models.ULIDParse("01F3DXRS0PNV978CZC2BFZYXWA")})

	dbQuery := db.Clauses(clause.OnConflict{DoNothing: true}).Create(&titles)
	fmt.Println("Titles added: " + strconv.FormatInt(dbQuery.RowsAffected, 10))
}
