package database

import (
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"
	"strconv"
)

func Paginate(c *gin.Context) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		start, _ := strconv.Atoi(c.Query("_start"))
		if start < 0 {
			start = 0
		}
		end, _ := strconv.Atoi(c.Query("_end"))
		if end <= 0 {
			end = 5
		}

		pageSize := end - start
		switch {
		case pageSize > 100:
			pageSize = 100
		case pageSize <= 0:
			pageSize = 5
		}
		return db.Offset(start).Limit(pageSize)
	}
}
