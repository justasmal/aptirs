package database

import (
	"aptirsapi/models"
	"errors"
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/plugin/dbresolver"
	"log"
	"strconv"
	"strings"
	"time"
)

var DB *gorm.DB

func getDialector(dsn string, dsnType string) gorm.Dialector {
	switch strings.ToLower(dsnType) {
	case "mysql":
		return mysql.Open(dsn)
	case "pgsql":
		return postgres.Open(dsn)
	}
	return nil
}

func InitDB(dsn string, dsnType string, sourcesCount int) *gorm.DB {
	var db = DB
	dsns := strings.Split(dsn, "|")

	if len(dsns) < 1 {
		panic(errors.New("no DSN found"))
	}
	sources := []gorm.Dialector{}
	replicas := []gorm.Dialector{}
	db, err := gorm.Open(getDialector(dsns[0], dsnType), &gorm.Config{})
	if err != nil {
		panic(err)
	}
	for i, dsnOne := range dsns {
		if i < sourcesCount-1 {
			sources = append(sources, getDialector(dsnOne, dsnType))
		} else {
			replicas = append(replicas, getDialector(dsnOne, dsnType))
		}
	}
	fmt.Println("Sources: " + strconv.Itoa(len(sources)) + ", Replicas: " + strconv.Itoa(len(replicas)))
	err = db.Use(dbresolver.Register(dbresolver.Config{
		Sources:  sources,
		Replicas: replicas,
		Policy:   dbresolver.RandomPolicy{},
	}))
	if err != nil {
		panic(err)
	}

	err = db.AutoMigrate(
		models.User{},
		models.Client{},
		models.Link{},

		models.Title{},
		models.Report{},
		models.SearchTerm{},
	)
	if err != nil {
		log.Fatalln(err)
	} else {
		log.Println("DB connection successful!")
	}
	sqlDB, err := db.DB()
	if err != nil {
		log.Fatalln(err)
	}
	if sqlDB != nil {
		sqlDB.SetMaxIdleConns(10)
		sqlDB.SetMaxOpenConns(100)
		sqlDB.SetConnMaxLifetime(time.Hour)
	}

	DB = db
	return DB
}

func GetDB() *gorm.DB {
	return DB
}
