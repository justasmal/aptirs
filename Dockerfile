FROM golang:1.16-alpine AS build

WORKDIR /src/

COPY go.mod .
COPY go.sum .

RUN go mod download


COPY . .
RUN CGO_ENABLED=0 GO111MODULE=on GOOS=linux GOARCH=amd64 go build -o /bin/aptirs

FROM scratch
COPY --from=build /bin/aptirs /bin/aptirs
ENTRYPOINT ["/bin/aptirs"]
EXPOSE 8080