package dto

type ReportField struct {
	Field string
	Value string
}

const StaticPart = "data.report_field."

func (rf *ReportField) GetForWebResponse() map[string]string {
	return map[string]string{
		"id":   rf.Field,
		"name": StaticPart + rf.Value,
	}
}

func (rf *ReportField) GetFromWebResponse(rfWebObject map[string]string) {
	rf.Field = rfWebObject["id"]
	for _, v := range ReportFields {
		if v.Field == rf.Field {
			rf.Value = v.Value
		}
	}
}

func (rf *ReportField) GetFromWebResponseString(rfWebObject string) {
	rf.Field = rfWebObject
	for _, v := range ReportFields {
		if v.Field == rf.Field {
			rf.Value = v.Value
		}
	}
}

func (rf *ReportField) GetReportResponseFromValue(rfWebObject string) {
	for _, v := range ReportFields {
		if v.Value == rfWebObject {
			rf.Value = v.Value
			rf.Field = v.Field
		}
	}
}

func (rf *ReportField) GetFieldName(rfWebObject string) string {
	for _, v := range ReportFields {
		if v.Value == rfWebObject {
			return v.Field
		}
	}
	return ""
}

var ReportFields = []ReportField{
	{Field: "Client", Value: "clientname"},
	{Field: "Title", Value: "titlename"},
	{Field: "URL", Value: "url"},
	{Field: "Linking URL", Value: "linkingurl"},
	{Field: "Link Status", Value: "status"},
	{Field: "Found at", Value: "datefound"},
	{Field: "Verified At", Value: "dateverified"},
	{Field: "Description", Value: "description"},
	{Field: "Verified By", Value: "username"},
}
