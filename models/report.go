package models

import (
	"gorm.io/datatypes"
	"gorm.io/gorm"
	"time"
)

type Report struct {
	ID       ULID           `json:"id" gorm:"primary_key;not null;unique"`
	Name     string         `json:"name" gorm:"size:255;not null"`
	Fields   datatypes.JSON `json:"fields" gorm:"not null"`
	ClientID ULID           `json:"client_id" gorm:"size:32;not null"`
	Client   *Client        `json:"client" gorm:"foreignKey:ClientID"`
}

func (u *Report) BeforeCreate(*gorm.DB) (err error) {
	u.ID = ULID.GenerateNew(u.ID)
	return nil
}

func (u *Report) AfterFind(tx *gorm.DB) (err error) {
	var client = Client{}
	tx.First(&client)
	u.Client = &client
	return
}

type ExportData struct {
	ReportID   string    `json:"report"`
	From       time.Time `json:"from"`
	To         time.Time `json:"to"`
	LinkStatus int       `json:"status"`
	FileType   string    `json:"type"`
}
