package models

import (
	"crypto/sha1"
	"encoding/base64"
	"encoding/json"
	"errors"
	"gorm.io/gorm"
	"regexp"
	"strings"
	"time"
)

type Link struct {
	ID         ULID      `json:"id" gorm:"primary_key;not null;unique"`
	URL        string    `json:"url" gorm:"size:2048;not null"`
	Title      *Title    `json:"title_info,omitempty" gorm:"foreignKey:TitleID"`
	TitleID    ULID      `json:"title" gorm:"not null"`
	Status     uint      `json:"status" gorm:"default:0;not null"`
	DateFound  time.Time `json:"date_found" gorm:"not null"`
	UniqueHash string    `json:"hash" gorm:"unique" gorm:"size:64;not null"`
	// optional fields
	DateVerified   *time.Time `json:"date_verified"`
	VerifiedBy     *User      `json:"-" gorm:"foreignKey:VerifiedById"`
	VerifiedById   *ULID      `json:"verified_by"`
	SearchTermUsed string     `json:"search_term" gorm:"size:255"`
	LinkingURL     string     `json:"linking_url" gorm:"size:2048"`
	Description    string     `json:"description" gorm:"size:2048"`
}

func (u *Link) BeforeCreate(*gorm.DB) (err error) {
	t := time.Now()
	u.ID = ULID.GenerateNew(u.ID)
	u.DateFound = t
	u.DateVerified = nil
	u.VerifiedBy = nil
	u.VerifiedById = nil
	return nil
}

func (u *Link) RegenID(*gorm.DB) (err error) {
	u.ID = ULID.GenerateNew(u.ID)
	return nil
}

type jsonStructForHash struct {
	Url   string `json:"url"`
	Title string `json:"title"`
}

func (u *Link) AfterFind(tx *gorm.DB) (err error) {
	var title = Title{}
	tx.First(&title)
	u.Title = &title
	if u.VerifiedById != nil {
		var verifiedBy = User{}
		tx.First(&verifiedBy)
		u.VerifiedBy = &verifiedBy
	}
	return
}

func preReplace(expr string, repl string, subject string) string {
	compile, _ := regexp.Compile(expr)
	return compile.ReplaceAllString(subject, repl)
}

func (u *Link) GenerateUniqueHash() (err error) {
	if u.Title.ID.String() == "" || u.URL == "" {
		return errors.New("url and title cant be null when creating new hash")
	}
	url := strings.ToLower(u.URL)
	url = preReplace("^http(s)://", "", url)
	url = preReplace("www\\.", "", url)
	url = preReplace("/$", "", url)
	bytes, err := json.Marshal(jsonStructForHash{
		Url:   url,
		Title: u.Title.ID.String(),
	})
	if err != nil {
		return err
	}
	hasher := sha1.New()
	hasher.Write(bytes)
	sha := base64.URLEncoding.EncodeToString(hasher.Sum(nil))
	u.UniqueHash = sha
	return
}
