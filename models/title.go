package models

import (
	"gorm.io/gorm"
)

type Title struct {
	ID          ULID         `json:"id" gorm:"primary_key;not null;unique"`
	TitleName   string       `json:"name" gorm:"size:255;not null"`
	ClientID    ULID         `json:"client_id" gorm:"size:32;not null"`
	SearchTerms []SearchTerm `json:",omitempty" gorm:"foreignKey:TitleID"`
	Client      *Client      `json:",omitempty"`
}

func (u *Title) BeforeCreate(*gorm.DB) (err error) {
	u.ID = ULID.GenerateNew(u.ID)
	return nil
}

func (u *Title) AfterFind(tx *gorm.DB) (err error) {
	var client = Client{}
	tx.First(&client)
	u.Client = &client
	return
}
