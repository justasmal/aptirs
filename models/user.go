package models

import (
	"aptirsapi/utils"
	"encoding/json"
	"gorm.io/gorm"
)

type User struct {
	UserDTO
	Password          string `json:"password" gorm:"size:255;not null"`
	ConfirmedPassword string `json:"confirm_password" gorm:"-"`
}

type UserDTO struct {
	ID    ULID   `json:"id" gorm:"primary_key;not null;unique"`
	Email string `json:"email" gorm:"size:255;not null"`
	Role  string `json:"role" gorm:"size:255;not null"`
	Name  string `json:"name" gorm:"size:255;not null"`
}

func (u *User) BeforeCreate(*gorm.DB) (err error) {
	u.ID = ULID.GenerateNew(u.ID)
	hash, _ := utils.GeneratePassword(u.Password)
	u.Password = hash
	if len(u.Role) == 0 {
		u.Role = "ROLE_USER"
	}
	return nil
}

func (u *User) ToDTO() UserDTO {
	var dtoData UserDTO
	data, _ := json.Marshal(u)
	_ = json.Unmarshal(data, &dtoData)
	return dtoData
}

func UserListToUserDTOList(users []User) []UserDTO {
	dtos := []UserDTO{}
	for _, user := range users {
		dtos = append(dtos, user.ToDTO())
	}
	return dtos
}
