package models

type SearchTerm struct {
	Term    string `json:"term" gorm:"size:255;not null;index:search_term,unique"`
	TitleID ULID   `json:"title" gorm:"size:32;not null;index:search_term"`
}
