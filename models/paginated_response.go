package models

type PaginatedResponse struct {
	//CurrentPage int64       `json:"page"`
	Meta Meta        `json:"meta"`
	Data interface{} `json:"data"`
}

type Meta struct {
	TotalItems int64 `json:"total"`
}
