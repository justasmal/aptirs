package models

import (
	"database/sql/driver"
	"encoding/json"
	"github.com/oklog/ulid/v2"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
	"math/rand"
	"sync"
	"time"
)

var ulidMutex sync.Mutex
var ulidEntropy = ulid.Monotonic(rand.New(rand.NewSource(time.Now().UnixNano())), 0)

type ULID ulid.ULID

func (my ULID) GormDBDataType(db *gorm.DB, field *schema.Field) string {
	switch db.Dialector.Name() {
	case "mysql", "sqlite":
		return "BINARY(16)"
	case "postgres":
		return "bytea"
	}
	return ""
}

func (my ULID) Value() (driver.Value, error) {
	return ulid.ULID(my).Value()
}

func ULIDParse(id string) ULID {
	return ULID(ulid.MustParse(id))
}

func ULIDStrictParse(id string) (ULID, error) {
	a, err := ulid.ParseStrict(id)
	return ULID(a), err
}

func (my ULID) String() string {
	return ulid.ULID(my).String()
}

func (my ULID) MarshalJSON() ([]byte, error) {
	s := ulid.ULID(my)
	str := "\"" + s.String() + "\""
	return []byte(str), nil
}

func (my *ULID) UnmarshalJSON(by []byte) error {
	var str string
	err := json.Unmarshal(by, &str)
	if err != nil {
		return err
	}
	*my = ULIDParse(str)
	return nil
}

func (my *ULID) Scan(value interface{}) error {
	u := ulid.ULID(*my)
	err := u.Scan(value)
	if err != nil {
		return err
	}
	*my = ULID(u)
	return err
}

func (my ULID) GenerateNew() ULID {
	equal := ulid.ULID(my).Compare(ulid.MustParse("00000000000000000000000000"))
	if equal == 0 {
		ulidMutex.Lock()
		defer ulidMutex.Unlock()
		return ULID(ulid.MustNew(ulid.Timestamp(time.Now()), ulidEntropy))
	}
	return my
}
