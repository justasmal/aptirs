package models

import (
	"encoding/json"
	"gorm.io/gorm"
)

type Client struct {
	ClientSimpleDTO
	Titles  []*Title  `json:",omitempty" gorm:"foreignKey:ClientID"`
	Reports []*Report `json:",omitempty" gorm:"foreignKey:ClientID"`
}

type ClientSimpleDTO struct {
	ID           ULID   `json:"id" gorm:"primary_key;unique"`
	Name         string `json:"name" gorm:"unique"`
	ContactEmail string `json:"email"`
	ContactPhone string `json:"phone"`
}

func (u *Client) BeforeCreate(db *gorm.DB) (err error) {
	u.ID = ULID.GenerateNew(u.ID)
	return nil
}

func (u *Client) ToSimpleDTO() ClientSimpleDTO {
	var dto ClientSimpleDTO
	data, _ := json.Marshal(u)
	_ = json.Unmarshal(data, &dto)
	return dto
}

func ClientListToToSimpleDTOList(clients []Client) []ClientSimpleDTO {
	dtos := []ClientSimpleDTO{}

	for _, client := range clients {
		dtos = append(dtos, client.ToSimpleDTO())
	}
	return dtos
}
