package main

import (
	"aptirsapi/routers"
	"aptirsapi/utils"
	"github.com/joho/godotenv"
	"os"
)

func main() {
	_ = godotenv.Load(".env")
	err := os.Setenv("TZ", "UTC")
	if err != nil {
		panic(err)
	}
	_, err = utils.GetEnv("DSN")
	if err != nil {
		panic(err)
	}
	_, err = utils.GetEnv("JWTSECRET")
	if err != nil {
		panic(err)
	}

	r, err := routers.GetRouters()
	if err != nil {
		panic(err)
	}
	err = r.Run()
	if err != nil {
		panic(err)
	}
}
